package codingame;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Mime {

	public static void main(String args[]) throws Exception {
		
       // Scanner in = new Scanner(System.in);
		Scanner in = new Scanner(new File("C:\\Users\\59013-57-14\\Desktop\\red\\codingame\\mime\\test1.txt"));
		
        int N = in.nextInt(); // Number of elements which make up the association table.
        int Q = in.nextInt(); // Number Q of file names to be analyzed.
        
        HashMap<String, String> ext = new HashMap<String, String>();
        for (int i = 0; i < N; i++) {
            String EXT = in.next(); // file extension
            String MT = in.next(); // MIME type.
            EXT = EXT.toLowerCase();

            ext.put(EXT,MT);
            
        }
       
        
        in.nextLine();
        
        for (int i = 0; i < Q; i++) {
            String FNAME = in.nextLine(); // One file name per line.
            
            String[] QB = FNAME.split("\\.");
   
            QB[(QB.length-1)] = QB[(QB.length-1)].toLowerCase();
        
            
            if(FNAME.charAt(FNAME.length()-1)=='.'){
            	System.out.println("UNKNOWN");
            }else if(QB.length == 1){
            	System.out.println("UNKNOWN");

            }else if(ext.get(QB[(QB.length-1)]) == null) {
            	System.out.println("UNKNOWN");
            }else {
            	System.out.println(ext.get(QB[(QB.length-1)]));
            }
            
            
        }

    


        // For each of the Q filenames, display on a line the corresponding MIME type. If there is no corresponding type, then display UNKNOWN.
    
    }
	
	
}
