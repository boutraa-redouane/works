package inviteCmd;
import inviteCmd.Clavier;
import java.io.File;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws Exception {

		boolean bool = true;
		String res = "";
		Clavier monClavier = new Clavier(args.length == 0);

		while(bool) {
			
			System.out.print(">> ");
			res = monClavier.saisirString();
			res = res.toLowerCase();

			if(res.startsWith("river")) {
				River exe = new River();
				exe.exec(res.split(" "));

			} else if(res.startsWith("isprime")) {
				Isprime exe = new Isprime();
				exe.exec(res.split(" "));

			}else if(res.equals("help")) {
				Help exe = new Help();
				exe.exec(args);
			
			}else if(res.equals("exit")) {
				Exit exe = new Exit();
				bool = exe.exec(res.split(" "));

			}else if(res.equals("pwd")) {
				Pwd exe = new Pwd();
				exe.exec(args);

			}else if(res.equals("history")) {
				History exe = new History();
				exe.exec(args);
			}else{ System.out.println("Commande introuvable"); } 

		}
		
	}
}
