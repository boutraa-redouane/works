package inviteCmd;

abstract class Cmd {
	
	public abstract boolean exec(String[] args);
	
	protected void ajouterALHistorique() {
		if(this instanceof Historizable) {
			History.cmdsList.add((Historizable)this);
		}
	}
	
}
