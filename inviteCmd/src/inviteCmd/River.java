package inviteCmd;
import inviteCmd.Cmd;

public class River extends Cmd {

	public boolean exec(String[] args) {
		
		boolean arg1=true;
		
		try {
			Integer.parseInt(args[1]);
			Integer.parseInt(args[2]);
		}catch(Exception e) {
			arg1 = false;
		}

		if(args.length == 3 && arg1) {
			int r1 = Integer.parseInt(args[1]);
			int r2 = Integer.parseInt(args[2]);

			int res = 0;
			String intStr = "";

			while(r1!=r2){

				res =0;

				if(r1<r2){
					intStr = "" + r1;
					for(int i = 0; i<intStr.length();i++){
						res = res + intStr.charAt(i)-48;
					}
					r1 = r1 + res;
				}

				res =0;
				if(r1>r2){
					intStr = "" + r2;
					for(int i = 0; i< intStr.length();i++){
						res = res + intStr.charAt(i)-48;

					}
					r2 = r2 + res;
				}


			}
			System.out.println(r1);
		}else {
			System.out.println("Les paramètres ne sont pas reconnu");
		}
		return false;
	}

}
