package animaux;

public abstract class Animal {
	
	public void repire() {
		System.out.println("respire");
	}
	
	public abstract void exec();

}
